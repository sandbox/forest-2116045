$(document).ready(function() {
$('#save_search').click(function() {
tag = $('input.search-tag').val();
$('#alert_message').empty();
$.ajax({
type: 'GET',
url: 'search_save',
data: 'tag=' + tag,
async: false,
success: function(msg) {
$('#alert_message').text(msg).show('slow');
}
}).responseText;
});

$('#share_search').click(function() {
tag = $('input.search-tag').val();
$('#alert_message').empty();
$.ajax({
type: 'GET',
url: 'search_share',
data: 'tag=' + tag,
async: false,
success: function(msg) {
$('#alert_message').text(msg).show('slow');
}
}).responseText;
});
});